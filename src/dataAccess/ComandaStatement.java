package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.Comanda;

public class ComandaStatement {

	JDBC driver name and database URL
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/STUDENTS";

   //  Database credentials
   static final String USER = "username";
   static final String PASS = "password";
   
   public static void main(String[] args) {
   @Test public void testAutomaticLogin() throws Exception {
  final String tableName = "automaticAllowedClients";
  // Avatica should log in for us with this info
  String url = jdbcUrl + ";principal=" + SpnegoTestUtil.CLIENT_PRINCIPAL + ";keytab="
      + clientKeytab;
  LOG.info("Updated JDBC url: {}", url);
  try (Connection conn = DriverManager.getConnection(url);
      Statement stmt = conn.createStatement()) {
    assertFalse(stmt.execute("DROP TABLE IF EXISTS " + tableName));
    assertFalse(stmt.execute("CREATE TABLE " + tableName + "(pk integer)"));
    assertEquals(1, stmt.executeUpdate("INSERT INTO " + tableName + " VALUES(1)"));
    assertEquals(1, stmt.executeUpdate("INSERT INTO " + tableName + " VALUES(2)"));
    assertEquals(1, stmt.executeUpdate("INSERT INTO " + tableName + " VALUES(3)"));

    ResultSet results = stmt.executeQuery("SELECT count(1) FROM " + tableName);
    assertTrue(results.next());
    assertEquals(3, results.getInt(1));
  }
}
	}

}
