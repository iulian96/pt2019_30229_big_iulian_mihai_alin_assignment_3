package dataAccess;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import connection.ConnectionFactory;
import model.Furnizor;

public class FurnizorStatement {

	public static List<ProductoUnidadMedida> selectAllProductoUnidadMedida(String condition) throws SQLException{
   	 Connection conect=ConnectionConfiguration.conectar();
String query = " select * from producto_unidad_medida "+condition;
		 Statement statement = null;
		 ResultSet rs=null;
		 List<ProductoUnidadMedida> objetos = new ArrayList<ProductoUnidadMedida>();
		 try {
			statement = conect.createStatement();
			rs=statement.executeQuery(query);
			while(rs.next()){
				ProductoUnidadMedida objeto = new ProductoUnidadMedida();
				
				objeto.setUnidadMedidaId(rs.getInt("unidad_medida_id"));
				objeto.setUnidadMedidaNombre(rs.getString("uni_nombre"));
				objetos.add(objeto);
			}
		}
		catch (SQLException e) {e.printStackTrace();}
		finally{
			if (statement != null) {statement.close();}
			if (conect != null) {conect.close();}
		}
		return objetos;
   }
}
