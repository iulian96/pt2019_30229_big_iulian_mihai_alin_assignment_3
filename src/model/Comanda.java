package model;

import java.util.*;

public class Comanda {

	private int item;
    private String desc;
    private double price;
     
    public Gadget(int i, String d, double p)
    {
     
        item=i;
        desc=d;
        price=p;
    }
 
 
    public int getItem()
    {
        return item;
    }
     
    public String getDesc()
    {
        return desc;
    }
     
    public double getPrice()
    {
        return price;
    }
}
	}

}
