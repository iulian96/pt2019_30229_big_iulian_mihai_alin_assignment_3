package bll;

import java.util.regex.Pattern;

import model.Client;


	public static boolean Validators(String email) {
   boolean result = true;
   try {
      InternetAddress emailAddr = new InternetAddress(email);
      emailAddr.validate();
   } catch (AddressException ex) {
      result = false;
   }
   return result;
}